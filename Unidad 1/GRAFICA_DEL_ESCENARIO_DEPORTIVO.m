%ESCENARIO DE CANCHA DEPORTIVA (ESCENARIO CANCHA DE BASQUET)
%20m*12m
y=[0:1:20]; %metros
x=[0:1:12]; %metros
[X,Y]=meshgrid(x,y);
%meshgrid nos sirve para sacar los puntos que se midieron de metro en metro en el area de trabajo
%Da a conocer que se genero una matriz de 20*12 es decir en total de 240 mediciones 

Z=[2 1 1 1 1 1 2 2 2 4 4 4 2;
   2 2 2 2 4 6 5 5 24 5 2 2 6;
   2 2 2 6 10 11 11 12 16 23 30 37 39;
   3 3 3 4 5 9 11 19 16 27 30 23 16;
   3 3 3 3 5 5 5 5 9 10 24 37 39;
   4 4 4 4 9 9 8 13 20 16 19 30 41;
   5 5 5 9 16 30 39 27 23 13 8 5 5;
   11 11 13 16 16 24 27 27 24 19 12 8 3;
   8 8 9 11 13 11 11 19 27 30 13 5 2;
   142 122 88 81 78 75 58 58 41 24 12 5 2;
   182 171 169 154 142 122 122 88 75 58 41 20 12;
   169 142 122 122 81 75 41 30 24 12 12 8 3;
   154 150 130 122 122 130 108 75 58 24 27 24 19;
   142 113 108 88 88 75 41 58 75 108 113 122 122;
   88 85 81 76 75 68 58 55 41 24 58 75 88;   
   5 12 24 41 41 58 75 78 81 88 88 108 122;
   16 12 13 16 16 19 12 16 19 24 30 41 55;
   11 10 8 7 7 5 5 5 4 4 4 7 10;
   10 10 9 9 7 6 4 4 4 4 3 3 2;
   9 8 8 8 6 6 6 6 4 4 3 2 2;
   1 1 2 2 3 3 3 2 2 2 1 1 0]; %VALORES MEDIDOS DEL LUXOMETRO

subplot(2,2,1)
surf(X,Y,Z)
title('Grafica de superficie')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,2)
contour(X,Y,Z)
title('Curvas de Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,3)
surfc(X,Y,Z)
title('Grafica de superficie y Curvas Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')