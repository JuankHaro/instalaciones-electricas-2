%AREA DE TRABAJO
%ESCENARIO DE LOCAL COMERCIAL INTERIOR
%5m*7m
y=[0:1:5]; %metros
x=[0:1:7]; %metros
[X,Y]=meshgrid(x,y);
%meshgrid nos sirve para sacar los puntos que se midieron de metro en metro en el area de trabajo
%Da a conocer que se genero una matriz de 8x6 es decir en total de 48 mediciones 

Z=[79 81 46 45 72 78 25 24;
    81 72 125 138 170 183 180 78;
    72 79 81 90 116 159 115 81;
    72 90 125 142 153 187 170 72;
    45 72 70 116 153 153 115 46;
    21 25 45 79 116 142 81 78]; %VALORES MEDIDOS DEL LUXOMETRO

subplot(2,2,1)
surf(X,Y,Z)
title('Grafica de superficie')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,2)
contour(X,Y,Z)
title('Curvas de Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,3)
surfc(X,Y,Z)
title('Grafica de superficie y Curvas Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')