%ESCENARIO DE VIA PUBLICA (ESCENARIO EXTERIOR)
%5m*5m
y=[0:1:5]; %metros
x=[0:1:5]; %metros
[X,Y]=meshgrid(x,y);
%meshgrid nos sirve para sacar los puntos que se midieron de metro en metro en el area de trabajo
%Da a conocer que se genero una matriz de 8x6 es decir en total de 48 mediciones 

Z=[6 10 12 8 7 5;
   7 11 19 12 4 3;
   11 11 16 12 6 2;
   48 44 42 27 4 1;
   39 37 31 19 7 4;
   5 12 19 6 4 2]; %VALORES MEDIDOS DEL LUXOMETRO

subplot(2,2,1)
surf(X,Y,Z)
title('Grafica de superficie')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,2)
contour(X,Y,Z)
title('Curvas de Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')

subplot(2,2,3)
surfc(X,Y,Z)
title('Grafica de superficie y Curvas Isolux')
xlabel('m')
ylabel('m')
zlabel('lx')